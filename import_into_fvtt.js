//var currentMonster = {"name":"Alghollthu Master (Aboleth)","relativeURL":"Monsters.aspx?ID=2","familyName":"Alghollthu","level":"7","alignment":"Lawful Evil","creatureType":"Aberration","size":"Huge","nethysUrl":"https://2e.aonprd.com/Monsters.aspx?ID=2","recallKnowledgeText":"Recall Knowledge (Occultism): DC 25","flavorText":"Aboleths form the core of alghollthu society, and while they are the “common folk” of their own societies, they see themselves as masters of all others. Unlike their leaders, who mask their actions using magical disguises, aboleths revel in their monstrous forms, appearing as primeval fish with tentacles. Masters of psychic manipulation, they are a species so ancient that they were present in the world when it was young, before the gods had turned their attention to the planet. They see all other life as something they have the right to control, so the idea that potential slaves might have faith in a higher power other than themselves enrages aboleths.<br><br>","rarity":"uncommon","traits":[{"traitName":"Aberration","traitSummary":"Aberrations are creatures from beyond the planes or corruptions of the natural order.","traitURL":"https://2e.aonprd.com/Traits.aspx?ID=1"},{"traitName":"Aquatic","traitSummary":"Aquatic creatures are at home underwater. Their bludgeoning and slashing unarmed Strikes don’t take the usual –2 penalty for being underwater. Aquatic creatures can breathe water but not air.","traitURL":"https://2e.aonprd.com/Traits.aspx?ID=168"}],"sourceDoc":"Bestiary pg. 14","perceptionMod":"+17","perceptionText":"darkvision","languagesList":"Aklo, Alghollthu, Aquan, Common, Undercommon","skills":[{"name":"Athletics","mod":"+16"},{"name":"Deception","mod":"+15"},{"name":"Intimidation","mod":"+15"},{"name":"Lore","mod":"+14","skillDetails":"any one subcategory"},{"name":"Occultism","mod":"+16"}],"attributes":{"str":"+5","dex":"+1","con":"+6","int":"+3","wis":"+5","cha":"+4"},"interactionAbilities":[{"name":"Mucus Cloud","actionCost":"passive","actionText":"(<a style=\"text-decoration:underline\" href=\"https://2e.aonprd.com/Traits.aspx?ID=46\">aura</a>, <a style=\"text-decoration:underline\" href=\"Traits.aspx?ID=206\">disease</a>) 5 feet. While underwater, an aboleth exudes a cloud of transparent slime. An air-breathing creature adjacent to an aboleth must succeed at a DC 25 Fortitude save each round or lose the ability to breathe air but gain the ability to breathe water for 3 hours."}],"AC":"23","saves":[{"name":"Fort","mod":"+15"},{"name":"Ref","mod":"+10"},{"name":"Will","mod":"+16"}],"HP":"135","speed":"10 feet","otherSpeeds":{"swim":"60 feet"},"attackActions":[{"name":"tentacle","actionCost":1,"modifier":"+16","traits":["agile","magical","reach 15 feet"],"damageRolls":["2d8+10 bludgeoning"],"attackEffects":["slime"]}],"spellAbilities":[{"name":"Occult Innate Spells","DC":"25","spellLevels":[{"spells":[{"spellName":"project image","nethysUrl":"https://2e.aonprd.com/Spells.aspx?ID=237","detailText":"(at will)"},{"spellName":"veil","nethysUrl":"https://2e.aonprd.com/Spells.aspx?ID=355","detailText":"(at will)"}],"level":"7th"},{"spells":[{"spellName":"dominate","nethysUrl":"https://2e.aonprd.com/Spells.aspx?ID=87","detailText":"(x3)"},{"spellName":"illusory scene","nethysUrl":"https://2e.aonprd.com/Spells.aspx?ID=161","detailText":"(at will)"}],"level":"6th"},{"spells":[{"spellName":"illusory object","nethysUrl":"https://2e.aonprd.com/Spells.aspx?ID=160","detailText":"(at will)"}],"level":"5th"},{"spells":[{"spellName":"hallucinatory terrain","nethysUrl":"https://2e.aonprd.com/Spells.aspx?ID=145","detailText":"(at will)"}],"level":"4th"},{"spells":[{"spellName":"hypnotic pattern","nethysUrl":"https://2e.aonprd.com/Spells.aspx?ID=157","detailText":"(at will)"}],"level":"3rd"}]}],"offensiveAbilities":[{"name":"Slime","actionCost":"passive","actionText":"(<a href=\"https://2e.aonprd.com/Traits.aspx?ID=38\" title=\"A curse is an effect that places some long-term affliction on a creature. Curses are always magical and are typically the result of a spell or trap.\"><u>curse</u></a>, <a href=\"Traits.aspx?ID=120\" title=\"This magic comes from the occult tradition, calling upon bizarre and ephemeral mysteries. Anything with this trait is magical.\"><u>occult</u></a>, <a href=\"Traits.aspx?ID=162\" title=\"Afflictions with the virulent trait are harder to remove. You must succeed at two consecutive saves to reduce a virulent affliction&#x2019;s stage by 1. A critical success reduces a virulent affliction&#x2019;s stage by only 1 instead of by 2.\"><u>virulent</u></a>) <b>Saving Throw</b> Fortitude DC 25; <b>Stage 1</b> no ill effect (1 round); <b>Stage 2</b> the victim’s skin softens, inflicting drained 1 (1 round); <b>Stage 3</b> the victim’s skin transforms into a clear, slimy membrane, inflicting drained 2 until the curse ends; every hour this membrane remains dry, the creature’s drained condition increases by 1 (permanent). A <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=251\">remove disease</a> spell can counteract this curse, but immunity to disease offers no protection against it."}],"imageFilename":"Alghollthu_AlghollthuMaster.png"};
// var currentMonster = {"name":"Adult Gold Dragon","relativeURL":"Monsters.aspx?ID=152","familyName":"Dragon, Metallic","level":"15","alignment":"Lawful Good","creatureType":"Dragon","size":"Huge","nethysUrl":"https://2e.aonprd.com/Monsters.aspx?ID=152","recallKnowledgeText":"Recall Knowledge (Arcana): DC 34","flavorText":"Gold dragons are the epitome of metallic dragonkind, unrivaled in their strength as well as their wisdom. They command the unwavering reverence of all other metallic dragons, who view gold dragons as their leaders and counselors. Golds rival the raw power of even red dragons, much to the chagrin of their chromatic cousins, and the two races are often regarded as bitter rivals. But despite their incredible power, gold dragons are fond of discourse and prefer to talk through solutions to problems rather than rely upon brute strength. Long-lived as they are, they necessarily take a wide view of all situations and never act without considering all possible options and outcomes. Because of this, gold dragons willingly converse with any creature that seeks them out, even evil chromatic dragons. Mortals might find this behavior strange, considering the longstanding war between chromatic and metallic dragons, but dragons know all too well that desperate situations sometimes call for drastic alliances. And although gold dragons might consider brief truces with their chromatic brethren in the case of world-ending threats, they also know when such alliances have run their course.<br><br> When another metallic dragon faces a quandary or a foe beyond its own ability to overcome, its best option is often to seek the counsel of the eternally wise and gloriously righteous gold dragons. Locating these legendary beings is no easy task, however, for gold dragons are notoriously reclusive. Their intellect and wisdom is such that they prefer to ponder the great questions of life in seclusion, where they strive to formulate solutions to the world’s most pressing problems. As a result, gold dragons are sometimes absent when metallic dragons gather together, or are missing from tribunals where their counsel would be beneficial. Impatient dragons sometimes begrudge gold dragons for this apparent unreliability, but such aspersions are usually a result of jealousy rather than any true criticism; in their hearts, other dragons know that few gold dragons purposefully exclude themselves from truly important matters.<br> A gold dragon’s incredible foresight and unparalleled enlightenment means they are unlikely to interfere in the business of individual mortals, though the rare person who captures the attention of a gold dragon is fortunate indeed, for there are few beings in the cosmos who can offer such prudent and considerate advice. Rulers and individuals in stations of high power have an easier time of garnering the aid of a gold dragon; entire wars have been avoided thanks to a gold dragon’s last-minute intermediation.<br> Gold dragons are often found in warm grasslands and savannas, lands where they can enjoy long, meditative flights without attracting the attention of potential enemies. They tend to sleep either out in the open in a barren, remote place, or within a heavily secreted or fortified lair, such as a forgotten sink hole or in the labyrinthine caverns of a terrestrial chasm. Gold dragons may enlist trusted servants and allies to guard their lairs, though many live truly solitary lives, preferring to protect their hoards with nonlethal traps and magical wards.<br><br>","rarity":"common","traits":[{"traitName":"Dragon","traitSummary":"Dragons are reptilian creatures, often winged or with the power of \u001e flight. Most are able to use a breath weapon and are immune to sleep and paralysis.","traitURL":"https://2e.aonprd.com/Traits.aspx?ID=50"},{"traitName":"Fire","traitSummary":"Effects with the fire trait deal fire damage or either conjure or manipulate fire. Those that manipulate fire have no effect in an area without fire. Creatures with this trait consist primarily of fire or have a magical connection to that element.","traitURL":"https://2e.aonprd.com/Traits.aspx?ID=72"}],"sourceDoc":"Bestiary pg. 124","perceptionMod":"+29","perceptionText":"darkvision, scent (imprecise) 60 feet","languagesList":"Common, Draconic, Dwarven, Elven, Sylvan","skills":[{"name":"Acrobatics","mod":"+22"},{"name":"Arcana","mod":"+24"},{"name":"Athletics","mod":"+28"},{"name":"Diplomacy","mod":"+29"},{"name":"Medicine","mod":"+27"},{"name":"Religion","mod":"+29"},{"name":"Society","mod":"+26"}],"attributes":{"str":"+7","dex":"+3","con":"+6","int":"+5","wis":"+6","cha":"+4"},"AC":"38","saves":[{"name":"Fort","mod":"+28"},{"name":"Ref","mod":"+25"},{"name":"Will","mod":"+28"},{"name":"all","saveDetail":"+1 status to all saves vs. magic"}],"weaknesses":"cold 15","immunities":"fire, paralyzed, sleep","HP":"330","defensiveAbilities":[{"name":"Frightful Presence","actionCost":"passive","actionText":"(<a style=\"text-decoration:underline\" href=\"https://2e.aonprd.com/Traits.aspx?ID=206\">aura</a>, <a style=\"text-decoration:underline\" href=\"Traits.aspx?ID=60\">emotion</a>, <a style=\"text-decoration:underline\" href=\"Traits.aspx?ID=68\">fear</a>, <a style=\"text-decoration:underline\" href=\"Traits.aspx?ID=106\">mental</a>) 90 feet, DC 33<br>(<a style=\"text-decoration:underline\" href=\"https://2e.aonprd.com/Traits.aspx?ID=206\">aura</a>, <a style=\"text-decoration:underline\" href=\"https://2e.aonprd.com/Traits.aspx?ID=60\">emotion</a>, <a style=\"text-decoration:underline\" href=\"https://2e.aonprd.com/Traits.aspx?ID=68\">fear</a>, <a style=\"text-decoration:underline\" href=\"https://2e.aonprd.com/Traits.aspx?ID=106\">mental</a>) A creature that first enters the area must attempt a Will save. Regardless of the result of the saving throw, the creature is temporarily immune to this monster’s Frightful Presence for 1 minute.<br><br> <b>Critical Success</b> The creature is unaffected by the presence.<br> <b>Success</b> The creature is <a style=\"text-decoration:underline\" href=\"https://2e.aonprd.com/Conditions.aspx?ID=19\">frightened 1</a>.<br> <b>Failure</b> The creature is <a style=\"text-decoration:underline\" href=\"https://2e.aonprd.com/Conditions.aspx?ID=19\">frightened 2</a>.<br> <b>Critical Failure</b> The creature is <a style=\"text-decoration:underline\" href=\"https://2e.aonprd.com/Conditions.aspx?ID=19\">frightened 4</a>."},{"name":"Attack of Opportunity","actionCost":"reaction","actionText":"Jaws only.<br><b>Trigger</b> A creature within the monster’s reach uses a manipulate action or a move action, makes a ranged attack, or leaves a square during a move action it’s using. <b>Effect</b> The monster attempts a melee Strike against the triggering creature. If the attack is a critical hit and the trigger was a manipulate action, the monster disrupts that action. This Strike doesn’t count toward the monster’s multiple attack penalty, and its multiple attack penalty doesn’t apply to this Strike."},{"name":"Golden Luck","actionCost":"reaction","actionText":"<b>Trigger</b> The gold dragon fails a saving throw. <b>Effect</b> The dragon improves its result by one degree of success, turning a failure into a success or a critical failure into a normal failure. The dragon can’t use this ability again for 1d4 rounds."}],"speed":"50 feet","otherSpeeds":{"fly":"180 feet","swim":"50 feet"},"attackActions":[{"name":"jaws","actionCost":1,"modifier":"+30","traits":["fire","magical","reach 15 feet"],"damageRolls":["3d12+15 piercing","3d6 fire"]},{"name":"claw","actionCost":1,"modifier":"+30","traits":["agile","magical","reach 10 feet"],"damageRolls":["3d10+15 slashing"]},{"name":"tail","actionCost":1,"modifier":"+28","traits":["magical","reach 20 feet"],"damageRolls":["3d10+13 slashing"]},{"name":"horns","actionCost":1,"modifier":"+28","traits":["agile","magical","reach 15 feet"],"damageRolls":["2d12+13 piercing"]}],"spellAbilities":[{"name":"Arcane Innate Spells","DC":"35","spellLevels":[{"spells":[{"spellName":"sunburst","nethysUrl":"https://2e.aonprd.com/Spells.aspx?ID=326"}],"level":"7th"},{"spells":[{"spellName":"locate","nethysUrl":"https://2e.aonprd.com/Spells.aspx?ID=173","detailText":"(gems only)"}],"level":"3rd"},{"spells":[{"spellName":"detect alignment","nethysUrl":"https://2e.aonprd.com/Spells.aspx?ID=65","detailText":"(evil only)"}],"level":"1st"}]}],"offensiveAbilities":[{"name":"Breath Weapon ","actionCost":2,"actionText":"The gold dragon breathes in one of two ways. The dragon can’t use Breath Weapon again for 1d4 rounds.<ul><li><b>Flame</b> (<a style=\"text-decoration:underline\" href=\"https://2e.aonprd.com/Traits.aspx?ID=11\">arcane</a>, <a style=\"text-decoration:underline\" href=\"Traits.aspx?ID=65\">evocation</a>,<a style=\"text-decoration:underline\" href=\"Traits.aspx?ID=72\">fire</a>); The dragon breathes a blast of flame in a 40-foot cone that deals 15d6 fire damage (DC 37 basic Reflex save).</li><li><b>Weakening Gas</b> (<a style=\"text-decoration:underline\" href=\"Traits.aspx?ID=11\">arcane</a>, <a style=\"text-decoration:underline\" href=\"Traits.aspx?ID=117\">necromancy</a>); The dragon breathes a blast of weakening gas. Each creature within a 40-foot cone must succeed at a DC 37 Fortitude save or become <a style=\"text-decoration:underline\" href=\"Conditions.aspx?ID=13\">enfeebled</a> 2 for 1 round (or enfeebled 3 on a critical failure).</li></ul>"},{"name":"Draconic Frenzy","actionCost":2,"actionText":"The gold dragon makes two claw Strikes and one horns Strike in any order"},{"name":"Draconic Momentum","actionCost":"passive","actionText":"When the gold dragon scores a critical hit with a strike, it recharges Breath Weapon."}],"sidebarText":"<h3 class=\"title\">Gold Dragon Spellcasters</h3>Gold dragon spellcasters tend to cast the following spells. Unlike most dragons, they cast divine spells instead of arcane.<br><br><b>Young Gold Dragon</b><br><b>Divine Prepared Spells</b> DC 29, attack +24; <b>4th</b> <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=74\"><i>discern lies</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=148\"><i>heal</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=258\"><i>restoration</i></a>; <b>3rd</b> <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=78\"><i>dispel magic</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=147\"><i>haste</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=148\"><i>heal</i></a>; <b>2nd</b> <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=258\"><i>restoration</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=256\"><i>resist energy</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=271\"><i>see invisibility</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=287\"><i>silence</i></a>; <b>1st</b> <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=7\"><i>alarm</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=189\"><i>mending</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=238\"><i>protection</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=302\"><i>spirit link</i></a>; <b>Cantrips (4th)</b> <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=61\"><i>daze</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=66\"><i>detect magic</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=126\"><i>forbidding ward</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=229\"><i>prestidigitation</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=246\"><i>read aura</i></a><br><br><b>Adult Gold Dragon</b><br><b>Divine Prepared Spells</b> DC 35, attack +30; As young gold dragon, plus <b>6th</b> <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=24\"><i>blade barrier</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=148\"><i>heal</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=344\"><i>true seeing</i></a>; <b>5th</b> <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=19\"><i>banishment</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=29\"><i>breath of life</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=272\"><i>sending</i></a>; <b>Cantrips (6th)</b> <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=61\"><i>daze</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=66\"><i>detect magic</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=126\"><i>forbidding ward</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=229\"><i>prestidigitation</i></a>, <a style=\"text-decoration:underline\" href=\"Spells.aspx?ID=246\"><i>read aura</i></a><h3 class=\"title\">Parnoneryx</h3>This adult gold dragon had a long and tragic existence. Formerly an ally of Iomedae, he was defeated by a powerful white dragon and imprisoned in ice. Though freed by the Glorious Reclamation, he was soon slain by evil adventurers who used his head to create a powerful weapon known as a tathlum&#x2014;which was used to lift the siege of Citadel Rivad and end the Glorious Reclamation.","imageFilename":"DragonGold_AncientGoldDragon.png"};
// var currentMonster = {"name":"Animated Armor","relativeURL":"Monsters.aspx?ID=19","familyName":"Animated Object","level":"2","alignment":"Neutral","creatureType":"Construct","size":"Medium","nethysUrl":"https://2e.aonprd.com/Monsters.aspx?ID=19","recallKnowledgeText":"Recall Knowledge (Arcana, Crafting): DC 16","flavorText":"Suits of animated armor see use both as guardians and as training partners in high-end martial academies able to afford the extravagance. They are most often found in wizard laboratories and ancient dungeons.<br><br>","rarity":"common","traits":[{"traitName":"Construct","traitSummary":"A construct is an artificial creature empowered by a force other than necromancy. Constructs are often mindless; they are immune to disease, the paralyzed condition, and poison; and they may have Hardness based on the materials used to construct their bodies. Constructs are not living creatures, nor are they undead. When reduced to 0 Hit Points, a construct creature is destroyed.","traitURL":"https://2e.aonprd.com/Traits.aspx?ID=35"},{"traitName":"Mindless","traitSummary":"A mindless creature has either programmed or rudimentary mental attributes. Most, if not all, of their mental ability modifiers are –5. They are immune to all mental effects.","traitURL":"https://2e.aonprd.com/Traits.aspx?ID=108"}],"sourceDoc":"Bestiary pg. 20","perceptionMod":"+6","perceptionText":"darkvision","skills":[{"name":"Athletics","mod":"+9"}],"attributes":{"str":"+3","dex":"-3","con":"+4","int":"-5","wis":"+0","cha":"-5"},"AC":"17","ACDetails":"(13 when broken); construct armor","saves":[{"name":"Fort","mod":"+10"},{"name":"Ref","mod":"+3"},{"name":"Will","mod":"+4"}],"immunities":"bleed, death effects, disease, doomed, drained, fatigued, healing, mental, necromancy, nonlethal attacks, paralyzed, poison, sickened, unconscious","HP":"20","HPDetails":"Hardness 9","defensiveAbilities":[{"name":"Construct Armor","actionCost":"passive","actionText":"Like normal objects, an animated armor has Hardness. This Hardness reduces any damage it takes by an amount equal to the Hardness. Once an animated armor is reduced to less than half its Hit Points, or immediately upon being damaged by a critical hit, its construct armor breaks and its Armor Class is reduced to 13."}],"speed":"20 feet","attackActions":[{"name":"glaive","actionCost":1,"modifier":"+11","traits":["deadly 1d8","forceful","magical","reach 10 feet"],"damageRolls":["1d8+4 slashing"]},{"name":"gauntlet","actionCost":1,"modifier":"+9","traits":["agile","free-hand","magical"],"damageRolls":["1d6+4 bludgeoning"]}]};

const importPF2eMonsters = function(jsonFile, monsterModuleName="pf2e_updatednpcsheet", monstersPackCollection="pf2e_beastiary1.beastiary1", spellsPackCollection="pf2e.spells-srd"){
	let fullPathName = "modules\\pf2e_beastiary1\\importJSON\\"+jsonFile;
	fetch(fullPathName).then(r => r.json()).then(async records => {
        for ( let data of records ) {
            await processMonster(data, monstersPackCollection, spellsPackCollection, monsterModuleName);
        }
    });
};

var processMonster = async function(currentMonster, monstersPackCollection="pf2e_beastiary1.beastiary1", spellsPackCollection="pf2e.spells-srd", monsterModuleName="pf2e_updatednpcsheet"){
	// let monsterModuleName = monstersPackCollection.split('.')[0];
	if (!monstersPack)
		var monstersPack = await game.packs.find(p => p.collection === monstersPackCollection)
	if(!spellsPack)
		var spellsPack = await game.packs.find(p => p.collection === spellsPackCollection);
	if(!spellsContent)
		var spellsContent = await spellsPack.getContent();

	let tempActor = {};

	tempActor.name = currentMonster.name;
	tempActor.type = 'npc';
	tempActor.flags = {};
	tempActor.flags[monsterModuleName] = {};
	if(currentMonster.imageFilename) tempActor.img = 'modules/pf2e_beastiary1/Images/'+currentMonster.imageFilename;

	let tempData = {};
	tempData.abilities = {};
	tempData.abilities.str = {'label':'Strength','mod':currentMonster.attributes.str}; //Number(currentMonster.attributes.str)
	tempData.abilities.con = {'label':'Constitution','mod':Number(currentMonster.attributes.con)};
	tempData.abilities.wis = {'label':"Wisdom",'mod':Number(currentMonster.attributes.wis)};
	tempData.abilities.int = {'label':"Intelligence",'mod':Number(currentMonster.attributes.int)};
	tempData.abilities.dex = {'label':"Dexterity",'mod':Number(currentMonster.attributes.dex)};
	tempData.abilities.cha = {'label':"Charisma",'mod':Number(currentMonster.attributes.cha)};

	tempData.attributes = {};
	tempData.attributes.hp = {'value':currentMonster.HP,'max':currentMonster.HP};
	tempData.attributes.init = {'value':Number(currentMonster.perceptionMod)};
	tempData.attributes.perception = {'value':Number(currentMonster.perceptionMod)};
	tempData.attributes.speed = {'value':currentMonster.speed};
	if(currentMonster.otherSpeeds){
		let speedIter = '';
		let keys = Object.keys(currentMonster.otherSpeeds);
		console.log('keys',keys);
		keys.forEach((key,index) => {
			console.log('i',index);
			speedIter+=key+" "+currentMonster.otherSpeeds[key]+( index<keys.length-1 ?', ':'');
		});
		console.log(speedIter);
		tempData.attributes.speed.special = speedIter;
	}
	tempData.attributes.ac = {'value':currentMonster.AC};
	if(currentMonster.ACDetails) tempData.attributes.ac.details = currentMonster.ACDetails;

	tempData.martial = {simple: { value: 0 }};

	if(currentMonster.spellAbilities){
		tempData.attributes.spelldc = {'dc':currentMonster.spellAbilities[0].DC}

		tempData.spells = {};
		for(let i=0;i<currentMonster.spellAbilities.length;i++){
			for(let j=0, currentLevel=currentMonster.spellAbilities[i].spellLevels[0];
				j<currentMonster.spellAbilities[i].spellLevels.length;j++,currentLevel=currentMonster.spellAbilities[i].spellLevels[j]){
				let levelNum = currentLevel.level.includes('Cantrip') ? 0 : currentLevel.level.match(/\d+/g)[0];
				if(currentLevel.detailText && currentLevel.detailText.includes('Focus Point')) levelNum=11;
				tempData.spells['spell'+levelNum] = {};
				if(currentLevel.numUses){
					tempData.spells['spell'+levelNum] = {'value':currentLevel.numUses, 'max': currentLevel.numUses};
				}
				if(currentLevel.detailText){
					tempData.spells['spell'+levelNum].detailText = currentLevel.detailText;
				}
			}
		}
	}

	//Bunch of properties to store in flags
	if(currentMonster.flavorText) tempActor.flags[monsterModuleName].flavorText = {'value': currentMonster.flavorText};
	if(currentMonster.nethysUrl) tempActor.flags[monsterModuleName].nethysUrl = {'value':currentMonster.nethysUrl};
	if(currentMonster.recallKnowledgeText) tempActor.flags[monsterModuleName].recallKnowledgeText = {'value': currentMonster.recallKnowledgeText};
	if(currentMonster.sidebarText) tempActor.flags[monsterModuleName].sidebarText = {'value': currentMonster.sidebarText};

	tempData.details = {};
	tempData.details.alignment = {'value':currentMonster.alignment};
	tempData.details.level = {'value':currentMonster.level};
	tempData.details.sourceDoc = {'value':currentMonster.sourceDoc};
	if(currentMonster.creatureType && currentMonster.creatureType !== '—'){
		tempData.details.type = {'value':currentMonster.creatureType};
	}
	if(currentMonster.familyName && currentMonster.familyName !== '—'){
		tempData.details.ancestry = {'value':currentMonster.familyName};
	}

	if(currentMonster.traits){
		tempActor.flags[monsterModuleName].traits = {'value': []};
		for(let i=0; i<currentMonster.traits.length;i++){
			tempActor.flags[monsterModuleName].traits.value.push(currentMonster.traits[i].traitName.toLowerCase());
		}
	}

	tempData.saves = {};
	tempData.saves.fortitude = {'value':Number(currentMonster.saves[0].mod)};
	if(currentMonster.saves[0].saveDetail) tempData.saves.fortitude.saveDetail = currentMonster.saves[0].saveDetail;
	tempData.saves.reflex = {'value':Number(currentMonster.saves[1].mod)};
	if(currentMonster.saves[1].saveDetail) tempData.saves.fortitude.saveDetail = currentMonster.saves[1].saveDetail;
	tempData.saves.will = {'value':Number(currentMonster.saves[2].mod)};
	if(currentMonster.saves[2].saveDetail) tempData.saves.fortitude.saveDetail = currentMonster.saves[2].saveDetail;
	if(currentMonster.saves[3]) tempActor.flags[monsterModuleName].allSaveDetail = {'value':currentMonster.saves[3].saveDetail};

	tempData.traits = {};
	{
		tempData.traits.size = {};
		tempData.traits.size.value = ((size) => {
			switch(currentMonster.size){
				case 'Tiny': return 'tiny';
				case 'Small': return 'sm';
				case 'Medium': return 'med';
				case 'Large': return 'lg';
				case 'Huge': return 'huge';
				case 'Gargantuan': return 'grg';
			}
		})(currentMonster.size);
	}
	console.log(currentMonster.size, tempData.traits.size.value);
		//Update prototype token size
	let tokenSize = 1;
	switch(currentMonster.size){
		case 'Tiny': 
			tokenSize = 0.75;
			break;
		case 'Small':
			tokenSize = 1;
			break;
		case 'Medium':
			tokenSize = 1;
			break;
		case 'Large':
			tokenSize = 2;
			break;
		case 'Huge':
			tokenSize = 3;
			break;
		case 'Gargantuan':
			tokenSize = 4;
			break;
	}
	tempActor.token = {width: tokenSize, height: tokenSize};

	if(currentMonster.perceptionText) tempData.traits.senses = {'value':currentMonster.perceptionText};
	if(currentMonster.languagesList) tempData.traits.languages = {'value': currentMonster.languagesList.split(/[,;]/g).map((txt)=>txt.trim()) }
	if(currentMonster.immunities) tempData.traits.di = {'textValue':currentMonster.immunities};
	if(currentMonster.resistances) tempData.traits.dr = {'textValue':currentMonster.resistances};
	if(currentMonster.weaknesses) tempData.traits.dv = {'textValue':currentMonster.weaknesses};

	tempActor.data = tempData;


	let testActor = await Actor.create(tempActor,{'temporary':false, 'displaySheet': false})



	//Spells
	if(currentMonster.spellAbilities){
		for(let i=0;i<currentMonster.spellAbilities.length;i++){
				for(let j=0, currentLevel=currentMonster.spellAbilities[i].spellLevels[0];
					j<currentMonster.spellAbilities[i].spellLevels.length;j++,currentLevel=currentMonster.spellAbilities[i].spellLevels[j]){

					let levelNum = currentLevel.level.includes('Cantrip') ? 0 : currentLevel.level.match(/\d+/g)[0];
					if(currentLevel.detailText && currentLevel.detailText.includes('Focus Point')){
						levelNum=11;
					}
					let spellsArray = currentLevel.spells;
					for(let k=0,currentSpell=spellsArray[0];k<spellsArray.length;k++,currentSpell=spellsArray[k]){
						// Look in the Compendium for a spell with the same nethysUrl

						let mySpell = spellsContent.find(spell => spell.data.data.source.value===currentSpell.nethysUrl);
						if(mySpell){
							let numTimes = 1;
							let moreTimes = undefined;
							if(currentSpell.detailText && (moreTimes=((/\(x(\d+)\)/g).exec(currentSpell.detailText)))!==undefined )
							if(moreTimes && moreTimes.length===2) numTimes = moreTimes[1];
							for(let addidx=0;addidx<numTimes;addidx++){
								let newSpell = await testActor.importItemFromCollection(spellsPackCollection,mySpell.id);
								console.log(currentSpell.spellName, addidx);
								if(currentSpell.detailText){
									newSpell = await newSpell.update({'id':newSpell.data.id,'data.spellInstanceText':currentSpell.detailText})
									if(!moreTimes || moreTimes[0]!==currentSpell.detailText){
										newSpell = await newSpell.update({'id':newSpell.data.id,'name':newSpell.name+' '+currentSpell.detailText});
									}
								}
								if(currentLevel.level.includes("Constant")){
									newSpell = await newSpell.update({'id':newSpell.data.id,'name':newSpell.name+' (Constant)'});
								}
								if(levelNum===11||levelNum===0){ //Add the spell level of focus spells or cantrips
									newSpell = await newSpell.update({'id':newSpell.data.id,'name':newSpell.name+' - '+currentLevel.level + (levelNum===11 ? ' level':'')});
								}
								console.log(newSpell.data.data.level.value, levelNum);
								if(newSpell.data.data.level.value !== Number(levelNum)){
									newSpell = await newSpell.update({'id':newSpell.data.id, data: { level: { value: levelNum } } });
								}
							}
						}
						else {
							// Need to add a new spell using just the name because I couldn't find it in the compendium
							let newSpell = {};
							newSpell.name = currentSpell.spellName 
								+ ((levelNum===11||levelNum===0) ? ' - '+currentLevel.level + (levelNum===11 ? ' level':''):'') 
								+ (currentLevel.level.includes("Constant") ? ' (Constant)':'');
							newSpell.type = 'spell';
							newSpell.data = {level: {value: levelNum} };
							let returnValue = await testActor.createOwnedItem(newSpell, {'displaySheet':false});
						}
					}
				}
		}
	}

	//Attacks
	const processAttack = async function(attack,actor,monsterModuleName="pf2e_updatednpcsheet"){
		

		let newItem = {}
		newItem.name = attack.name;
		newItem.type = 'melee';
		newItem.data = {};
		newItem.data.bonus = {'value':Number(attack.modifier), total: Number(attack.modifier) }; //attack.modifier
		newItem.flags = {};
		newItem.flags[monsterModuleName] = {};

		const dmgTxtToObject = function(damageText){
			let damageArray = damageText.split(' ');
			let damageDie = '';
			let damageType = '';
			let finishedNum = false;
			let invDmgRegEx = /[^0123456789d+-]/g;
			for(let i=0;i<damageArray.length;i++){
				if(!damageArray[i].match(invDmgRegEx) && !finishedNum){
					damageDie+= damageArray[i];
				}else{
					finishedNum=true;
					damageType+= damageArray[i];
				}
			}
			console.log('damageDie',damageDie);
			console.log('damageType',damageType);
			return {'die': damageDie.trim(), 'damageType': damageType.trim()};

		}
		if(!(attack.damageRolls && attack.damageRolls.length>0)){
			newItem.data.damage = {};
			newItem.data.damageRolls = [];
		}else{
			let primaryDamage = dmgTxtToObject(attack.damageRolls[0]);
			newItem.data.damage = primaryDamage;

			newItem.flags[monsterModuleName].damageRolls = [];
			//For the future!!
			for(let i=0;attack.damageRolls&&i<attack.damageRolls.length;i++){
				let cDamageRoll = attack.damageRolls[i];
				newItem.flags[monsterModuleName].damageRolls.push(dmgTxtToObject(cDamageRoll));
			}
		}


		newItem.flags[monsterModuleName].attackEffects = [];
		for(let i=0;attack.attackEffects&&i<attack.attackEffects.length;i++){
			let cEffect = attack.attackEffects[i];
			if(cEffect!=="")
				newItem.flags[monsterModuleName].attackEffects.push(cEffect);
		}

		let attackTraitTxt = '';
		if(attack.traits && attack.traits.length > 0){
			attackTraitTxt+=" (";
		}
		newItem.data.traits = {value: []};
		for(let i=0;attack.traits&&i<attack.traits.length;i++){
			let cTrait = attack.traits[i];
			if(cTrait!==""){
				attackTraitTxt+=cTrait+(i<attack.traits.length-1 ? ', ':'');
				if(!newItem.data.traits.value.includes(cTrait))
					newItem.data.traits.value.push(cTrait);
			}
		}
		if(attack.traits && attack.traits.length > 0){
			attackTraitTxt+=")";
		}	

		// Commented out since NPC sheet has trait buttons now
		// newItem.name+=attackTraitTxt;
		newItem.data.description = {'value': attackTraitTxt.trim()};

		console.log(JSON.stringify(newItem,null,2));


		let returnValue = await actor.createOwnedItem(newItem, {'displaySheet':false})
		console.log(returnValue);
		//I have to update this after it returns because .damageRolls isn't a normal property for a 'melee' (I think)

		// return returnValue.update({'id':returnValue.id,'data.damageRolls':newItem.data.damageRolls, 'data.attackEffects':newItem.data.attackEffects})
		return returnValue;
		
	};

	if(currentMonster.attackActions){
		for(let i=0;i<currentMonster.attackActions.length;i++){
			await processAttack(currentMonster.attackActions[i],testActor,monsterModuleName);
		}
	}

	//Items
	if(currentMonster.items){
		for(let i=0;i<currentMonster.items.length;i++){
			let cItem = currentMonster.items[i];
			let oItem = {'name': cItem.name, 'type': 'equipment', 'data':{'description': {'type': "String", "label": "Description", "value": cItem.itemDetails || ""} } };
			await testActor.createOwnedItem(oItem, {displaySheet: false});
		}
	}
	//Actions
	const processAction = async function(action,actionType,actor,monsterModuleName="pf2e_updatednpcsheet"){
		let newAction = {
			'name':action.name,
			'type':'action', 
			'data': {
				'actionType': {'value': 'action'},
				'description': {'value': action.actionText},
				'traits': {value: []}
			}
		};

		newAction.flags = {};
		newAction.flags[monsterModuleName] = {};
		newAction.flags[monsterModuleName].npcActionType = {value: actionType};
		switch (action.actionCost){
			case 1:
			case 2:
			case 3:
				newAction.data.actions = {'value': action.actionCost}
				break;
			case 'freeAction':
				newAction.data.actionType.value = 'free';
				break;
			case 'reaction':
				newAction.data.actionType.value = 'reaction'
				break;
			default:
				newAction.data.actionType.value = 'passive'
		}
		let traitRegEx = /(?:Traits.aspx.+?">)(?:<\w+>)*(.+?)(?:<\/\w+>)*(?:<\/a>)/g;
		let matchTraits = [...action.actionText.matchAll(traitRegEx)];

		for(let i=0;i<matchTraits.length;i++){
			if(matchTraits[i] && matchTraits[i].length >= 2 && matchTraits[i][1]){
				console.log("Adding",matchTraits[i][1],"to",newAction.data.traits.value,newAction.data.traits.value.includes(matchTraits[i][1]));
				if(!newAction.data.traits.value.includes(matchTraits[i][1]))
					newAction.data.traits.value.push(matchTraits[i][1]);
			}
		}

		let returnValue = await actor.createOwnedItem(newAction, {'displaySheet': false});

		return returnValue;//.update({'id':returnValue.id, 'data.npcActionType':{'value':actionType}});
	}

	for(let i=0;currentMonster.interactionAbilities&&i<currentMonster.interactionAbilities.length;i++){
		await processAction(currentMonster.interactionAbilities[i],'interaction',testActor,monsterModuleName);
	}
	for(let i=0;currentMonster.defensiveAbilities&&i<currentMonster.defensiveAbilities.length;i++){
		await processAction(currentMonster.defensiveAbilities[i],'defensive',testActor,monsterModuleName);
	}
	for(let i=0;currentMonster.offensiveAbilities&&i<currentMonster.offensiveAbilities.length;i++){
		await processAction(currentMonster.offensiveAbilities[i],'offensive',testActor,monsterModuleName);
	}



	//Skills
	const processSkillAsLore = async function(skill,actor){
		let newAction = {
			'name':skill.name,
			'type':'lore', 
			'data': {
				'mod': { value: Number(skill.mod)},
			}
		};
		if(skill.skillDetails){
			newAction.name+=" ("+skill.skillDetails+")";
		}
		let returnValue = await actor.createOwnedItem(newAction, {'displaySheet': false});

		return returnValue.update({'id': returnValue.id, 'data.mod': { value: Number(skill.mod) } } );
	}

	for(let i=0;currentMonster.skills&&i<currentMonster.skills.length;i++){
		await processSkillAsLore(currentMonster.skills[i],testActor);
	}

	//Stuff not in basic pf2eActor
	{
		let updateObj = {'id':testActor.id, 'data': {}};

		// updateObj.data.nethysUrl = {'value':currentMonster.nethysUrl};
		// if(currentMonster.recallKnowledgeText) updateObj.data.recallKnowledgeText = {'value': currentMonster.recallKnowledgeText};
		// if(currentMonster.flavorText) updateObj.data.flavorText = {'value': currentMonster.flavorText};
		// if(currentMonster.sidebarText) updateObj.data.sidebarText = {'value': currentMonster.sidebarText};
		if(currentMonster.ACDetails){
			updateObj.data.attributes = {};
			updateObj.data.attributes.ac = {};
			updateObj.data.attributes.ac.details = currentMonster.ACDetails;
		}
		if(currentMonster.HPDetails){
			if(!updateObj.data.attributes) updateObj.data.attributes = {};
			updateObj.data.attributes.hp = {};
			updateObj.data.attributes.hp.details = currentMonster.HPDetails;
		}

		await testActor.update(updateObj);
	}
	await monstersPack.importEntity(testActor);
	testActor.delete();
}