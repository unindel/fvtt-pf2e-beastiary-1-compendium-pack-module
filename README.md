Monster importer and Compendium pack for Pathfinder 2 FVTT system.

Version 0.1.1

Patch Notes:
0.1.1 - Fixed many issues, updated scope for flag to point to updated npc sheet module.

Upcoming plans:
- Review processed monsters for issues.
- Scale images so that they're square/don't get squashed by FVTT


NPC sheet split into separate project at https://gitlab.com/unindel/fvtt-pf2e-updated-npc-sheet